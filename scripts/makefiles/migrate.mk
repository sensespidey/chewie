# Setup and perform migrations.

.PHONY: setup setup-real migrate migrate-real migrate-rollback migrate-rollback-real

setup:
		@$(MAKE-QUIET) setup-real
setup-real:
		@$(ECHO) "$(YELLOW)Beginning setup of multilingual migrations.$(RESET)"
		$(DRUSH) pm-enable ingredients -y $(QUIET)
		$(DRUSH) pm-enable ingredients_migrate -y $(QUIET)
		$(DRUSH) migrate-status
		@$(ECHO) "$(YELLOW)Completed migration setup.$(RESET)"

migrate:
		@$(MAKE-QUIET) migrate-real
migrate-real:
		@$(ECHO) "$(YELLOW)Beginning migrations.$(RESET)"
		$(DRUSH) migrate-import --all $(QUIET)
		@$(ECHO) "$(YELLOW)Finished migrations.$(RESET)"
