# Chewie Lando Site (with Drumkit)

This is a simple Drupal 8 repository to demonstrate the use of
[Drumkit](https://drumk.it) and [Lando](https://devwithlando.io) for local
development.

It is referenced from my blog post [here](https://consensus.enterprises/blog/lando-drumkit-setup).
