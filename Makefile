include .mk/GNUmakefile
include scripts/makefiles/*.mk
.PHONY: all

all:
		@$(MAKE-QUIET) start
		@$(MAKE-QUIET) build
		@$(MAKE-QUIET) install
